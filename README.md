#Comando para provisionar maquina AWS
ansible-playbook /ansible/playbook.yml --extra-vars "host=aws" -i /ansible/inventory/hosts
#Comando para provisionar maquina Devel
ansible-playbook /ansible/playbook.yml --extra-vars "host=webservers" -i /ansible/inventory/hosts
#Comando para provisionar todos los grupos
ansible-playbook /ansible/playbook.yml --extra-vars "host=all" -i /ansible/inventory/hosts